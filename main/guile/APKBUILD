# Maintainer: Natanael Copa <ncopa@alpinelinux.org>

# NOTE: Please do not push changes to this package without requesting a test run
# across all supported architectures.  Untested changes to this package may be
# reverted at any time, at the core team's discretion.

pkgname=guile
pkgver=2.0.14
pkgrel=0
pkgdesc="portable, embeddable Scheme implementation written in C"
url="https://www.gnu.org/software/guile/"
arch="all"
options="!check" # Requires LC_COLLATE & LC_MONETARY, failed w/ musl-locales.
license="LGPL-3.0-or-later AND GPL-3.0-or-later"
subpackages="$pkgname-dev $pkgname-doc $pkgname-libs"
makedepends="gmp-dev libtool ncurses-dev texinfo libunistring-dev libffi-dev
	gc-dev gawk"
depends_dev="guile gc-dev"
source="https://ftp.gnu.org/gnu/guile/guile-$pkgver.tar.gz
	0002-Mark-mutex-with-owner-not-retained-threads-test-as-u.patch
	"

case "$CARCH" in
x86 | x86_64 | mips64) options="$options !strip" ;;
# Otherwise we'll get strip: Unable to recognise the format of the input file
esac

build() {
	./configure \
		--build="$CBUILD" \
		--host="$CHOST" \
		--prefix=/usr \
		--disable-error-on-warning \
		--disable-static
	make
}

package() {
	make DESTDIR="$pkgdir" install
	rm "$pkgdir"/usr/lib/charset.alias
}

doc() {
	default_doc
	license="GFDL-1.3-or-later"
}

sha512sums="
d69c9bdf589fedcc227f3203012f6ed11c327cef3a0147d8e016fe276abecdb4272625efe1d0c7aa68219fe8f29bbced44089a4b479e4eafe01976c6b2b83633  guile-2.0.14.tar.gz
54a9fe0fa2ea83da7ae61f96612e3e653ec80f60ab41d1995dc44bd05c7ff68cc4fab36a655e8835c4ab1cf0966765a299ef2d73cb9f69d3ef955e6aeaa8062d  0002-Mark-mutex-with-owner-not-retained-threads-test-as-u.patch
"
